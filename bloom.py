# -*- coding: utf-8 -*-
#argparse allows the parsing of command line arguments
import argparse
#utility functions for the bloom filter project
import bfProjectUtils as util
#numpy numerical library - use for random values only
import numpy.random as random
#USAGE :
#random.seed(x) : sets seed to x
#random.randint(a,b) : returns random integer in range [a,b) (inclusive beginning and exclusive ending values)
#DO NOT IMPORT NUMPY other than the function above


class HashType1(object):
    #Implement Hash Type 1

    def __init__(self, config, genHashes):
        """
        Args:
            config (dictionary) : contains configuration data for this hashing object
            genHashes (boolean) : whether or not to generate new hash seeds/coefficients
                                    (Task 2 provides hash seeds/coefficients,
                                    Tasks 1 and 3 require you to make them yourself)
        """
        self.k = config['k']
        self.n = config['n']
        #set random seed to be generated seed at config load
        random.seed(config['genSeed'])
        #if generate new random hashes
        if genHashes:
            #build seed list self.seeds
            self.seeds = []
            for i in range(self.k):
                seed_i = random.randint(self.n)
                self.seeds.append(seed_i)

        #if not gen (task 2), then use hashes that are in config dictionary
        else:
            self.seeds = config['seeds']

    def getHashList(self, x):
        """
        Return list of k hashes of the passed integer, using self.seeds[i]+x to
        seed random number gen to give i'th hash value
        Args :
            x (int) : element to hash
        Returns list of k hashes of this element
        """
        res = []

        for i in range(self.k):
            random.seed(self.seeds[i] + x)
            rand_i = random.randint(self.n)
            res.append(rand_i)

        return res


class HashType2(object):
    #Implement Hash Type 2

    def __init__(self, config, genHashes):
        """
        Args:
            config (dictionary):contains configuration data for this hashing object
            genHashes (boolean) : whether or not to generate new hash seeds/coefficients
                                    (Task 2 provides hash seeds/coefficients,
                                    Tasks 1 and 3 require you to make them yourself)
        """
        self.k = config['k']
        self.n = config['n']
        self.N = config['N']

        self.P = util.findNextPrime(self.N)
        #generate new random hashes, or not if task2
        if genHashes:
            #set random seed to be generated seed at config load
            random.seed(config['genSeed'])
            #build lists of coefficients self.a and self.b
            self.a = []
            self.b = []
            for i in range(self.k):
                self.a.append(random.randint(1, self.n))
                self.b.append(random.randint(self.n))

        #if not gen (task 2), then use hashes that are in config dictionary
        else:
            self.a = config['a']
            self.b = config['b']

    def getHashList(self, x):
        """
        Return list of k hashes of the passed integer, using
        (self.a[i] * x + self.b[i] mod P) mod n
        to give i'th hash value - remember P and n must be prime, and P >= N

        Args :
            x (int) : element to hash
        Returns list of k hashes of this element
        """
        res = []

        for i in range(self.k):
            hash_i = ((((self.a[i] * x) + self.b[i]) % self.P) % self.n)
            res.append(hash_i)

        return res


class BloomFilter(object):
    #Implement a Bloom Filter data structure."""

    def __init__(self, config):
        """
        Args:
            config (dictionary): Configuration of this bloom filter
            config['N'] (int) universe size
            config['m'] (int) number of elements to add to filter
            config['n'] (int) number of bits in bloom filter storage array
            config['k'] (int) number of hash functions to use
            config['task'] (int) the task this bloom filter is to perform (1,2,3)
            config['type'] (int) type of hash function (1, 2, -1==unknown type)
            if type 1 hash :
                config['seeds'] (list of k ints) : seed values for k hash functions for type 1 hash function
            if type 2 hash :
                config['a'] (list of k ints) : a coefficients for k hash functions for type 2 hash function
                config['b'] (list of k ints) : b coefficients for k hash functions for type 2 hash function

            genHashes (boolean) : whether or not to generate new hash seeds/coefficients
                                    (Task 2 provides hash seeds/coefficients,
                                    Tasks 1 and 3 require you to make them yourself)
        """
        #task this boom filter is performing
        self.task = config['task']
        #task 1 and 3 require generated seeds for hashes, task 2 uses provided seeds/coefficients
        genHashes = (self.task != 2)
        #type of hash for this bloom filter
        self.type = config['type']

        # Make sure the size of the bloom filter is prime
        if not util.checkIfPrime(config['n']):
            config['n'] = util.findNextPrime(config['n'])

        if(self.type == 1):
            self.hashFunc = HashType1(config,genHashes)
        elif(self.type == 2):
            self.hashFunc = HashType2(config,genHashes)
        #elif(self.type == 3): #add your own hashes
        else:
            print('BloomFilter for task ' + str(self.task) + ' ctor : Unknown Hash type : ' + str(self.type))

        #your code goes here
        self.hash_table = [0] * config['n']

    def add(self, x):
        """Adds x to the data structure, using self.hashFunc
        Args:
            x (int): The integer to add to the bloom filter
        """
        for hash_i in self.hashFunc.getHashList(x):
            self.hash_table[hash_i] = 1

    def contains(self, x):
        """Indicates whether data structure contains x, using self.hashFunc, with the possibility of false positives

        Args:
            x (int): The integer to test.
        Returns:
            True or False whether structure contains x or not
        """
        for hash_i in self.hashFunc.getHashList(x):
            if self.hash_table[hash_i] != 1:
                return False

        return True


"""
function will take data list, insert first m elements into bloom filter, and check all elements in datalist for membership, returning a list of results of check
    Args :
        data (list) : list of integer data to add
        bf (object) : bloom filter object
        m (int) : number of elements to add to bloom filter from data (first m elements)
    Returns :
        list of results of checking

"""
def testBF(data, bf, m):
    #add first m elements
    for i in range(0,m):
        bf.add(data[i])
    print('Finished adding '+ str(m) +' integers to bloom filter')
    resList =[]
    #test membership of all elements
    for i in range(0,len(data)):
        resList.append(str(bf.contains(data[i])))
    return resList


"""
function will support required test for Task 2.  DO NOT MODIFY.
    configData : dictionary of configuration data required to build and test bloom filter
"""
def task2(configData):
    #instantiate bloom filter object
    bf = BloomFilter(configData)

    #bfInputData holds a list of integers.  Using these values you must :
    #   insert the first configData['m'] of them into the bloom filter
    #   test all of them for membership in the bloom filter
    bfInputData = util.readIntFileDat(configData['inFileName'])
    if(len(bfInputData) == 0):
        print('No Data to add to bloom filter')
        return
    else:
        print('bfInputData has '+str(len(bfInputData)) +' elements')
    #testBF will insert elements and test membership
    outputResList = testBF(bfInputData, bf, configData['m'])
    #write results to output file
    util.writeFileDat(configData['outFileName'],outputResList)
    #load appropriate validation data list for this hash function and compare to results
    util.compareResults(outputResList,configData)
    print('Task 2 complete')

"""
these two functions are added for your convenience, if you choose to use this code to perform tasks 1 and 3
"""
def task1(configData):
    #if you wish to use this code to perform task 1, you may do so
    #NOTE : task 1 does not require you to instantiate a bloom filter

    # Added Imports for plotting
    import matplotlib.pyplot as plt

    # Per instructions only use one hash function per hash type
    configData['k'] = 1

    hash_type_1 = HashType1(configData, True)
    hash_type_2 = HashType2(configData, True)

    input_data = util.readIntFileDat(configData['inFileName'])

    # Sample to make the scatter legible
    input_data_sample = input_data[:10000]
    h1_values, h2_values = [], []

    load_hash_1 = [0] * configData['n']
    load_hash_2 = [0] * configData['n']

    for data_x in input_data_sample:
        h1_hash = hash_type_1.getHashList(data_x)[0]
        h1_values.append(h1_hash)

        h2_hash = hash_type_2.getHashList(data_x)[0]
        h2_values.append(h2_hash)

        # Capture the load for each of the bloom filter entries
        load_hash_1[h1_hash] += 1
        load_hash_2[h2_hash] += 1

    count_load = [0] * 3
    for bucket_idx, bucket_load in enumerate(load_hash_1):
        count_load[bucket_load] += 1

    print('Load Counts for Hash Type 1:')
    for bucket_idx, bucket_load in enumerate(count_load):
        print('Bucket {} - Load {} ({}%)'.format(bucket_idx, bucket_load, 100.0*bucket_load/configData['n']))

    count_load = [0] * 3
    for bucket_idx, bucket_load in enumerate(load_hash_2):
        count_load[bucket_load] += 1

    print('Load Counts for Hash Type 2:')
    for bucket_idx, bucket_load in enumerate(count_load):
        print('Bucket {} - Load {} ({}%)'.format(bucket_idx, bucket_load, 100.0*bucket_load/configData['n']))

    plt.scatter(input_data_sample, h1_values, s=1)
    plt.axis((0, max(input_data_sample), 0, max(h1_values)))
    plt.title('Hash Type 1 - First 10K')
    plt.xlabel('Data Value')
    plt.ylabel('Hash Value')
    # plt.show()
    plt.savefig("t1_ht1.png", bbox_inches="tight")
    plt.close()

    buckets = list(range(len(load_hash_1)))
    plt.plot(buckets, load_hash_1)
    plt.title('Hash Type 1 - Load - First 10K')
    plt.xlabel('Bucket Number (1e6)')
    plt.ylabel('Load')
    plt.axis((0, len(buckets), 0, max(load_hash_1)))
    plt.yticks(range(0, max(load_hash_1) + 1))
    plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
    # plt.show()
    plt.savefig("t1_ht1_load.png", bbox_inches="tight")
    plt.close()

    # Hash Type 2
    plt.scatter(input_data_sample, h2_values, s=1)
    plt.axis((0, max(input_data_sample), 0, max(h2_values)))
    plt.title('Hash Type 2 - First 10K')
    plt.xlabel('Data Value')
    plt.ylabel('Hash Value')
    # plt.show()
    plt.savefig("t1_ht2.png", bbox_inches="tight")
    plt.close()

    buckets = list(range(len(load_hash_2)))
    plt.plot(buckets, load_hash_2)
    plt.title('Hash Type 2 - Load - First 10K')
    plt.xlabel('Bucket Number (1e6)')
    plt.ylabel('Load')
    plt.axis((0, len(buckets), 0, max(load_hash_2)))
    plt.yticks(range(0, max(load_hash_2) + 1))
    plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
    # plt.show()
    plt.savefig("t1_ht2_load.png", bbox_inches="tight")
    plt.close()

    task1_even_sample(configData)

    print('Task 1 complete')


def task1_even_sample(configData):
    # Added Imports for plotting
    import matplotlib.pyplot as plt

    # Per instructions only use one hash function per hash type
    configData['k'] = 1

    hash_type_1 = HashType1(configData, True)
    hash_type_2 = HashType2(configData, True)

    input_data = util.readIntFileDat(configData['inFileName'])

    # Sample to make the scatter legible
    input_data_sample = []
    num = 0
    for data in input_data:
        if data % 2 == 0:
            input_data_sample.append(data)
            num += 1
        if num == 10000:
            break

    h1_values, h2_values = [], []

    load_hash_1 = [0] * configData['n']
    load_hash_2 = [0] * configData['n']

    for data_x in input_data_sample:
        h1_hash = hash_type_1.getHashList(data_x)[0]
        h1_values.append(h1_hash)

        h2_hash = hash_type_2.getHashList(data_x)[0]
        h2_values.append(h2_hash)

        # Capture the load for each of the bloom filter entries
        load_hash_1[h1_hash] += 1
        load_hash_2[h2_hash] += 1

    count_load = [0] * 3
    for bucket_idx, bucket_load in enumerate(load_hash_1):
        count_load[bucket_load] += 1

    print('Load Counts for Hash Type 1 - Even:')
    for bucket_idx, bucket_load in enumerate(count_load):
        print('Bucket {} - Load {} ({}%)'.format(bucket_idx, bucket_load, 100.0*bucket_load/configData['n']))

    count_load = [0] * 3
    for bucket_idx, bucket_load in enumerate(load_hash_2):
        count_load[bucket_load] += 1

    print('Load Counts for Hash Type 2 - Even:')
    for bucket_idx, bucket_load in enumerate(count_load):
        print('Bucket {} - Load {} ({}%)'.format(bucket_idx, bucket_load, 100.0*bucket_load/configData['n']))

    plt.scatter(input_data_sample, h1_values, s=1)
    plt.axis((0, max(input_data_sample), 0, max(h1_values)))
    plt.title('Hash Type 1 - First 10K - Even')
    plt.xlabel('Data Value')
    plt.ylabel('Hash Value')
    # plt.show()
    plt.savefig("t1_ht1_even.png", bbox_inches="tight")
    plt.close()

    buckets = list(range(len(load_hash_1)))
    plt.plot(buckets, load_hash_1)
    plt.title('Hash Type 1 - Load - First 10K - Even')
    plt.xlabel('Bucket Number (1e6)')
    plt.ylabel('Load')
    plt.axis((0, len(buckets), 0, max(load_hash_1)))
    plt.yticks(range(0, max(load_hash_1) + 1))
    plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
    # plt.show()
    plt.savefig("t1_ht1_load_even.png", bbox_inches="tight")
    plt.close()

    # Hash Type 2
    plt.scatter(input_data_sample, h2_values, s=1)
    plt.axis((0, max(input_data_sample), 0, max(h2_values)))
    plt.title('Hash Type 2 - First 10K - Even')
    plt.xlabel('Data Value')
    plt.ylabel('Hash Value')
    # plt.show()
    plt.savefig("t1_ht2_even.png", bbox_inches="tight")
    plt.close()

    buckets = list(range(len(load_hash_2)))
    plt.plot(buckets, load_hash_2)
    plt.title('Hash Type 2 - Load - First 10K - Even')
    plt.xlabel('Bucket Number (1e6)')
    plt.ylabel('Load')
    plt.axis((0, len(buckets), 0, max(load_hash_2)))
    plt.yticks(range(0, max(load_hash_2) + 1))
    plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
    # plt.show()
    plt.savefig("t1_ht2_load_even.png", bbox_inches="tight")
    plt.close()

def construct_bloom_filter(hash_type, c, k, configData):
    # Initialize parameters
    configData['type'] = hash_type
    configData['k'] = k
    configData['n'] = util.findNextPrime(configData['m'] * c)

    # Every Bloom Filter generates its own unique hashes
    # The seed for each Bloom Filter needs to be unique
    configData['genSeed'] = random.randint(configData['N'])

    return BloomFilter(configData)

def calculate_false_positive_rate(hash_type, c, k, configData, input_data):
    # Import Numpy for randomizing the test data
    from numpy.random import shuffle
    from numpy import std
    num_trials = 10
    false_positive_rates = []
    m = configData['m']

    # Average false positive rate over 10 trials
    for _ in range(num_trials):
        # Construct empty Bloom Filter for each iteration
        bloom_filter = construct_bloom_filter(hash_type, c, k, configData)
        # Shuffle the elements to test different values with each iteration
        shuffle(input_data)

        num_false_positives = 0
        for i in range(m):
            bloom_filter.add(input_data[i])
        for i in range(m, len(input_data)):
            if bloom_filter.contains(input_data[i]):
                num_false_positives += 1
        false_positive_rates.append(float(num_false_positives) / float(len(input_data) - m))

    average_false_positives = sum(false_positive_rates) / float(num_trials)
    fp_std = std(false_positive_rates)
    return average_false_positives, fp_std

def task3(configData):
    #if you wish to use this code to perform task 3, you may do so
    #NOTE task 3 will require you to remake your bloom filter multiple times to perform the appropriate trials
    #this will necessitate either making a new bloom filter constructor or changing the config dictionary to
    #hold the appropriate values for k and n (filter size) based on c value, derived as in the notes
    #REMEMBER for type 2 hashes n must be prime.  util.findNextPrime(n) is provided for you to use to find the next largest
    #prime value of some integer.

    # Locally importing numpy for graphing purposes
    from numpy import exp
    from numpy import arange
    from numpy import array
    # Added Imports for plotting
    import matplotlib.pyplot as plt

    # There are no duplicates in testInput.txt
    input_data = util.readIntFileDat(configData['inFileName'])

    hash_type_values = [1, 2]
    c_values = [10, 15]

    results = {}
    results_std = {}

    k_values = []
    k_values_theoretical = []
    for c in c_values:
        ks = []
        for k in range(int(0.4*c), c+1, 1):
            ks.append(k)
        k_values.append(ks)
        k_min, k_max = min(ks), max(ks)
        ks_theory = []
        for ks in arange(k_min, k_max + 0.1, 0.1):
            ks_theory.append(ks)
        k_values_theoretical.append(ks_theory)

    for hash_type in hash_type_values:
        results[hash_type] = {}
        results_std[hash_type] = {}
        for c_i, c in enumerate(c_values):
            results[hash_type][c] = []
            results_std[hash_type][c] = []
            for k in k_values[c_i]:
                pass
                fp_rate, fp_std = calculate_false_positive_rate(hash_type, c, k, configData, input_data)
                results[hash_type][c].append(fp_rate)
                results_std[hash_type][c].append(fp_std)

    theoretical_results = {}
    for c_i, c in enumerate(c_values):
        theoretical_results[c] = []
        k_min, k_max = min(k_values[c_i]), max(k_values[c_i])
        for k in arange(k_min, k_max + 0.1, 0.1):
            theoretical_results[c].append((1.0 - exp(-(float(k)/float(c))))**k)

    import numpy as np
    np.save('results.npy', results)
    np.save('results_std.npy', results_std)
    results = np.load('results.npy').item()
    results_std = np.load('results_std.npy').item()

    for hash_type in hash_type_values:
        for c_i, c in enumerate(c_values):
            plot_fp_rate, = plt.plot(k_values[c_i], results[hash_type][c], label='False Positive Rate')
            max_std = array(results[hash_type][c]) + array(results_std[hash_type][c])
            min_std = array(results[hash_type][c]) - array(results_std[hash_type][c])
            plot_fp_std = plt.fill_between(k_values[c_i], max_std, min_std, alpha=0.1, color='blue')
            plot_theoretical_fp_rate, = plt.plot(k_values_theoretical[c_i], theoretical_results[c],
                                                 label='Theoretical False Positive Rate')
            optimal_k_value = c*np.log(2)
            optimal_fp_value = ((1.0 - exp(-(float(optimal_k_value)/float(c))))**optimal_k_value)
            optimal_point, = plt.plot(optimal_k_value, optimal_fp_value, 'r*', ms=15.0)
            plt.title('False Positive Rate - Hash Type={} C={}'.format(hash_type, c))
            plt.xlabel('k - Number of Hash Functions')
            plt.ylabel('False Positive Rate')
            plt.legend([plot_fp_rate, plot_fp_std, plot_theoretical_fp_rate, optimal_point],
                    ['FP Rate', 'FP Rate StD', 'Theoretical FP Rate', 'Optimal K Value'])
            plt.savefig('t3_ht{}_c{}.png'.format(hash_type, c), bbox_inches="tight")
            # plt.show()
            plt.close()

    print('Task 3 complete')

"""
main
"""
def main():
    #DO NOT REMOVE ANY ARGUMENTS FROM THE ARGPARSER BELOW
    parser = argparse.ArgumentParser(description='BloomFilter Project')
    parser.add_argument('-c', '--configfile',  help='File holding configuration of Bloom Filter', default='testConfigHashType2.txt', dest='configFileName')
    parser.add_argument('-i', '--infile',  help='Input file of data to add to Bloom Filter', default='testInput.txt', dest='inFileName')
    parser.add_argument('-o', '--outfile',  help='Output file holding Bloom Filter results', default='testOutput.txt', dest='outFileName')
    #you may use this argument to distinguish between tasks - default is task 2 - do not change
    #you are not required to use this code template for tasks 1 and 3.
    parser.add_argument('-t', '--task',  help='Which task to perform (1,2,3)', type=int, choices=[1, 2, 3], default=2, dest='taskToDo')
    parser.add_argument('-v', '--valfile',  help='Validation file holding Bloom Filter expected results', default='validResHashType2.txt', dest='valFileName')

    #args for autograder, do not modify
    parser.add_argument('-n', '--sName',  help='Student name, used by autograder', default='GT', dest='studentName')
    parser.add_argument('-x', '--autograde',  help='Autograder-called (2) or not (1=default)', type=int, choices=[1, 2], default=1, dest='autograde')
    args = parser.parse_args()

    #configData is a dictionary that holds the important info needed to build your bloom filter
    #this includes the hash functon coefficients for the hash calculation for Task 2 (do not use these for Tasks 1 and 3)
    #buildBFConfigStruct prints out to the console all the elements in configData
    configData = util.buildBFConfigStruct(args)

    #perform appropriate task - 2 is default and the task2 code execution will be tested for your grade
    if configData['task'] == 2 :
        task2(configData)
    elif configData['task'] == 1 :
        #you are not required to use this code template for tasks 1 and 3.
        task1(configData)
    elif configData['task'] == 3 :
        #you are not required to use this code template for tasks 1 and 3.
        task3(configData)
    else:
        print('Unknown Task : ' + str(configData['task']))


if __name__ == '__main__':
    main()
